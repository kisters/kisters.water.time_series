Stores Backends
===============

.. toctree::
   :maxdepth: 4

   memory_store
   file_store
   kiwis_store
   store_decorators
