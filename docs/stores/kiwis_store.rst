KiWIS Store
===========

KiWISStore class grants access to the time series data of the Kisters Web Interoperability Solution backend.

.. autoclass:: kisters.water.time_series.kiwis.KiWISStore
   :members:
   :undoc-members:
   :show-inheritance:
