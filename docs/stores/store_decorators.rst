TimeSeriesStoreDecorators
=========================

The TimeSeriesStoreDecorators allow you to "wrap" a TimeSeriesStore to extend its functionality.

.. autoclass:: kisters.water.time_series.store_decorators.AddMetadataStore
   :show-inheritance:

.. autoclass:: kisters.water.time_series.store_decorators.CacheStore
   :show-inheritance:
