import pytest

from kisters.water.time_series.test import (
    OldTimeSeriesClientAsyncTest,
    OldTimeSeriesClientCommentsTest,
    TimeSeriesClientAsyncTest,
    TimeSeriesClientCommentsTest,
    TimeSeriesClientSyncTest,
    TimeSeriesStoreBasicTest,
)


@pytest.mark.usefixtures("memory_store_cls")
class TestBasicMemoryStore(TimeSeriesStoreBasicTest):
    """"""


@pytest.mark.usefixtures("memory_store_cls")
class TestAsyncMemoryClientOld(OldTimeSeriesClientAsyncTest):
    """"""


@pytest.mark.usefixtures("memory_store_cls")
class TestCommentsMemoryClientOld(OldTimeSeriesClientCommentsTest):
    """"""


@pytest.mark.usefixtures("memory_store_cls")
class TestAsyncMemoryClient(TimeSeriesClientAsyncTest):
    """"""


@pytest.mark.usefixtures("memory_store_cls")
class TestSyncMemoryClient(TimeSeriesClientSyncTest):
    """"""


@pytest.mark.usefixtures("memory_store_cls")
class TestCommentsMemoryClient(TimeSeriesClientCommentsTest):
    """"""
