from datetime import datetime, timezone

import numpy as np
import pandas as pd
import pytest

from kisters.water.time_series.core import TimeSeriesMetadata
from kisters.water.time_series.parquet import ParquetStore
from kisters.water.time_series.test import (
    OldTimeSeriesClientAsyncTest,
    TimeSeriesClientAsyncTest,
    TimeSeriesClientSyncTest,
    TimeSeriesStoreBasicTest,
)


@pytest.mark.usefixtures("parquet_store_cls")
class TestBasicParquetStore(TimeSeriesStoreBasicTest):
    """"""


@pytest.mark.usefixtures("parquet_store_cls")
class TestAsyncParquetClientOld(OldTimeSeriesClientAsyncTest):
    """"""


@pytest.mark.usefixtures("parquet_store_cls")
class TestAsyncParquetClient(TimeSeriesClientAsyncTest):
    """"""


@pytest.mark.usefixtures("parquet_store_cls")
class TestSyncParquetClient(TimeSeriesClientSyncTest):
    """"""


async def test_check_consistency():
    ts_path = "test"
    start = datetime(year=2020, month=1, day=1, tzinfo=timezone.utc)
    end = datetime(year=2020, month=1, day=3, tzinfo=timezone.utc)

    index = pd.date_range(start, end, freq="5min", tz="utc")
    df = pd.DataFrame({"value": np.linspace(0, 100, index.shape[0])}, index=index)

    ts_metadata = TimeSeriesMetadata(path=ts_path)

    store = ParquetStore("test.pq")
    async with store as client:
        await client.create_time_series(ts_metadata)
        await client.write_data_frame({ts_path: df})

    del store

    store = ParquetStore("test.pq")
    async with store as client:
        df2 = await client.read_data_frame(ts_path)
    pd.testing.assert_frame_equal(df, df2, check_freq=False)

    store._client._filename.unlink()
