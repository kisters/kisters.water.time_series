from datetime import datetime, timezone

from kisters.water.time_series.core import EnsembleMember, TimeSeriesKey


def test_ensemble_member():
    em = EnsembleMember()
    now = datetime.now(timezone.utc)
    assert em.copy_with(t0=now) == EnsembleMember(t0=now)
    assert em.copy_with(member="foo") == EnsembleMember(member="foo")


def test_time_series_key():
    tsk = TimeSeriesKey(path="foo")
    now = datetime.now(timezone.utc)
    assert tsk.copy_with(t0=now) == TimeSeriesKey(path="foo", t0=now)
    assert tsk.copy_with(member="foo") == TimeSeriesKey(path="foo", member="foo")
    assert tsk == eval(repr(tsk))
    tsk2 = TimeSeriesKey(path="foo", t0=now, dispatch_info="foo", member="foo")
    assert tsk2 == eval(repr(tsk2))
