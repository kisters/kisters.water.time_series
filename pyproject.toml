[build-system]
requires = ["setuptools>=61", "wheel", "setuptools_scm[toml]>=6.2"]

[tool.setuptools_scm]
# equivalent to use_scm_version = True

[tool.setuptools.packages]
find = {namespaces = true, include = ["kisters.*"]}

[project]
dynamic = ["version"]
name = "kisters.water.time_series"
description = "KISTERS Time Series Access library"
keywords = ["kisters", "time series", "time", "series"]
readme = "README.md"
requires-python = ">=3.9"
authors = [{name = "Alberto Sabater", email = "alberto.sabatermorales@kisters.de"}]
license = {text = "LGPL"}
classifiers = [
    "Development Status :: 4 - Beta",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
]
dependencies = [
    "httpx",
    "orjson",
    "pandas",
    "pyarrow",
    "pydantic",
    "typing-extensions",
]

[project.optional-dependencies]
test = ["pytest", "pytest-asyncio", "pytest-cov"]
mypy = ["mypy", "pandas-stubs", "pyarrow", "pytest"]
docs = ["sphinx", "sphinx-rtd-theme"]

[project.urls]
Homepage = "https://gitlab.com/kisters/time-series/kisters.water.time_series"
Documentation = "https://kisterswatertime-series.readthedocs.io/en/latest/"
Repository = "https://gitlab.com/kisters/time-series/kisters.water.time_series.git"

[tool.black]
line-length = 108

[tool.isort]
profile = "black"
line_length = 108
known_first_party = "kisters"

[tool.mypy]
packages = ["kisters"]
plugins = ["pydantic.mypy"]
# --strict
disallow_any_generics = true
disallow_subclassing_any = true
disallow_untyped_calls = true
disallow_untyped_defs = true
disallow_incomplete_defs = true
check_untyped_defs = true
disallow_untyped_decorators = true
no_implicit_optional = true
warn_redundant_casts = true
warn_unused_ignores = true
warn_return_any = true
implicit_reexport = false
strict_equality = true
# --strict end

[tool.pydantic-mypy]
init_forbid_extra = true
init_typed = true
warn_required_dynamic_aliases = true
warn_untyped_fields = true

[tool.pytest.ini_options]
addopts = "-vx"
asyncio_mode = "auto"

[tool.ruff]
line-length = 108
exclude = ["docs/**", "examples/**"]
[tool.ruff.lint]
select = [
    "ASYNC", # flake8-async
    "B",     # flake8-bugbear
    "C4",    # flake8-comprehensions
    "DTZ",   # flake8-datetimez
    "E",     # pycodestyle errors
    "EM",    # flake8-errmsg
    "F",     # pyflakes
    "G",     # flake8-logging-format
    "I",     # isort
    "NPY",   # NumPy
    "RET",   # flake8-return
    "SIM",   # flake8-simplify
    "W",     # pycodestyle warnings
]

ignore = [
    "E501", # line too long, handled by black
    "B905", # explicit strict for zip, need to support python before 3.10
]
